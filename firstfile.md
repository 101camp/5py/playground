```
192:playground qinbo$ git branch -a
* master
  remotes/origin/HEAD -> origin/master
  remotes/origin/bob_lzy
  remotes/origin/bruceq
  remotes/origin/bruceq619
  remotes/origin/carefreelee
  remotes/origin/dboder
  remotes/origin/ddgwang
  remotes/origin/gargoyle101
  remotes/origin/gyg101
  remotes/origin/heyi
  remotes/origin/iboyangcong
  remotes/origin/ichme
  remotes/origin/juliazh
  remotes/origin/kitty_branch
  remotes/origin/kittypang
  remotes/origin/liuyilc
  remotes/origin/lixiaoyao
  remotes/origin/lixiaoyao101
  remotes/origin/master
  remotes/origin/mikecheng1992
  remotes/origin/msyixin
  remotes/origin/mumafeiya
  remotes/origin/new-kitty
  remotes/origin/niuliming
  remotes/origin/ooheyhoney
  remotes/origin/pangzi
  remotes/origin/py101
  remotes/origin/qq704195100
  remotes/origin/shankai2018
  remotes/origin/test_kity
  remotes/origin/tonony
  remotes/origin/tonony101
  remotes/origin/vapaus101
  remotes/origin/vcssin
  remotes/origin/wonius
  remotes/origin/xuwenisme
  remotes/origin/zht789oo
192:playground qinbo$ git checkout dboder
Branch 'dboder' set up to track remote branch 'dboder' from 'origin'.
Switched to a new branch 'dboder'
192:playground qinbo$ git branch
* dboder
  master
192:playground qinbo$ git status
On branch dboder
Your branch is up to date with 'origin/dboder'.

nothing to commit, working tree clean
192:playground qinbo$ ls
LPTHW		README.md
192:playground qinbo$ pwd
/Users/didi/Documents/Gitlab/playground
192:playground qinbo$ touch firstfile.md
192:playground qinbo$ git add .
192:playground qinbo$ git commit -a
[dboder 939e24f] first commit
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 firstfile.md
192:playground qinbo$ git push origin dboder
warning: redirecting to https://gitlab.com/101camp/5py/playground.git/
Enumerating objects: 4, done.
Counting objects: 100% (4/4), done.
Delta compression using up to 4 threads
Compressing objects: 100% (2/2), done.
Writing objects: 100% (3/3), 300 bytes | 300.00 KiB/s, done.
Total 3 (delta 0), reused 1 (delta 0)
remote: 
remote: To create a merge request for dboder, visit:
remote:   https://gitlab.com/101camp/5py/playground/-/merge_requests/new?merge_request%5Bsource_branch%5D=dboder
remote: 
To https://gitlab.com/101camp/5py/playground
   311beb6..939e24f  dboder -> dboder
192:playground qinbo$ git checkout master
Switched to branch 'master'
Your branch is up to date with 'origin/master'.
192:playground qinbo$ git merge dboder
fatal: refusing to merge unrelated histories
192:playground qinbo$ git merge dboder --allow-unrelated-histories
CONFLICT (add/add): Merge conflict in README.md
Auto-merging README.md
Automatic merge failed; fix conflicts and then commit the result.
192:playground qinbo$ echo $?
1
192:playground qinbo$ git branch
  dboder
* master
192:playground qinbo$ git merge dboder --allow-unrelated-histories
error: Merging is not possible because you have unmerged files.
hint: Fix them up in the work tree, and then use 'git add/rm <file>'
hint: as appropriate to mark resolution and make a commit.
fatal: Exiting because of an unresolved conflict.
192:playground qinbo$ ls
LPTHW		carefreelee	gargoyle101	liuyilc		vcssin		yixin
README.md	cindy		heyi		mikecheng1992	woniu		zht
bob_lzy		ddgwang		juliazh		pangzi		xuwenisme
bruceq619	firstfile.md	kittypang	ss.md		yikeming
192:playground qinbo$ git add .
192:playground qinbo$ git merge dboder --allow-unrelated-histories
fatal: You have not concluded your merge (MERGE_HEAD exists).
Please, commit your changes before you merge.
192:playground qinbo$ git commit -a
[master 6d27cbe] Merge branch 'dboder'
192:playground qinbo$ git merge dboder --allow-unrelated-histories
Already up to date.
192:playground qinbo$ git push origin master
warning: redirecting to https://gitlab.com/101camp/5py/playground.git/
Enumerating objects: 7, done.
Counting objects: 100% (7/7), done.
Delta compression using up to 4 threads
Compressing objects: 100% (3/3), done.
Writing objects: 100% (3/3), 610 bytes | 610.00 KiB/s, done.
Total 3 (delta 2), reused 0 (delta 0)
To https://gitlab.com/101camp/5py/playground
   dc9fb3e..6d27cbe  master -> master
```
