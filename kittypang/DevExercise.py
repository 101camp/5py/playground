import sys

print ("你好，欢迎来到单词闯关魔鬼训练营！\n")
print ("接下来，你需要连续闯关，且中途不能退出\n")
print ("你准备好了吗？\n")

while 1:
    ChooseYesorNo = input("请输入 yes/no：")    
    if ChooseYesorNo == 'no': 
        sys.exit(0)
    elif ChooseYesorNo == 'yes': 
        print ("\n闯关开始，祝你好运！\n")
        break
    else:
        print ("\n请重新输入。\n")
        break


print ("【第一关】请回答：“美国”用英文怎么说？\n")
while 1:
    TypeEnglishWords = input("请输入：")    
    if TypeEnglishWords == 'america': 
        print ("\n接近了，再考虑一下大小写？\n")
    elif TypeEnglishWords == 'America': 
        print ("\n正确！恭喜你闯关成功！\n")
        print ("当前进度：1/4 关\n")
        break
    else:
        print ("\n再想想，还有什么单词表达这个意思？\n")
        break

print ("【第二关】请回答：“星期三”用英文怎么说？\n")
while 1:
    TypeEnglishWords = input("请输入：")    
    if TypeEnglishWords == 'wednesday': 
        print ("\n接近了，再考虑一下大小写？\n")
    elif TypeEnglishWords == 'Wednesday': 
        print ("\n正确！恭喜你闯关成功！\n")
        print ("当前进度：2/4 关\n")
        break
    else:
        print ("\n再想想，还有什么单词表达这个意思？\n")

print ("【第三关】请回答：表示“再来一次”的英文单词是什么？\n")
while 1:
    TypeEnglishWords = input("请输入：")    
    if TypeEnglishWords == 'again': 
        print ("\n正确！恭喜你闯关成功！\n")
        print ("当前进度：3/4 关\n")
        print ("还剩最后一关，加油！\n")
        break
    elif TypeEnglishWords == 'Again': 
        print ("\n接近了，再考虑一下大小写？\n")
    else:
        print ("\n再想想，还有什么单词表达这个意思？\n")

print ("【Boss关】请回答：“大妈”的英文名是什么？\n")
while 1:
    TypeEnglishWords = input("请输入：")    
    if TypeEnglishWords == 'Zoom.Quiet': 
        print ("\n正确！恭喜你闯完全部关卡！\n")
        print ("有缘再见！\n")
        break
    elif TypeEnglishWords == 'zoomquiet':
        print ("\n接近了，再考虑一下大小写？\n")
    elif TypeEnglishWords == 'Zoomquiet':
        print ("\n接近了，再考虑一下大小写？\n")
    elif TypeEnglishWords == 'ZoomQuiet':
        print ("\n差一点就成功了，再想想？\n")
    else:    
        print ("\n再想想？\n")

