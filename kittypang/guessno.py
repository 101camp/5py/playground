from random import uniform
from sys import exit


myNumber = int(uniform(2,99))  # 随机在2~99 之间产生一个整数, 赋值myNumber 
# print ("When input ctrl+c, Will exit game.")    # 给一个提示: ctrl+C 退出, 需要写代码如何退出, 强制退出不友好.



print("\n 你好，欢迎来到猜数字游戏！游戏规则如下：\n")
print("游戏会随机生成（1～99）之间的神秘数字，你需要猜这个数字是什么。\n")
print("你可以输入任意次直到你猜中为止。任何时候ctrl+c 退出游戏\n")    # 有一个问题 ， ctrl+c的退出的包error code如何规避？

def start():
    choice = int(input("请输入一个（1～99）之间的数字 > "))
    if  1 <= choice and choice <= 99:
        urNumber = choice
        start_game(urNumber,myNumber)
    else:
        dead("你猜的数字不在1～99之间，游戏结束")

def dead(why):
    print(why,"GoodBye")
    exit(0)

def start_game(urNumber,myNumber):
        if urNumber < myNumber:
            print("... (~_~) 猜小了")
            start()
        elif urNumber > myNumber:
            print ("...(~_-) 猜大了")
            start()
        else:
            print ("恭喜你，猜对了！")
            print("继续新一轮猜数字吗？ (Yes/No) >", end='')
            next_game = input ()
            if next_game.upper() == "YES":    #  转换成大写，则忽略输入的大小写差异
                myNumber = int (uniform(2,99))    ### 有一个bug， 下一轮时不能重新产生一个随机数
                start()
            else:
                exit(0)

start()








"""
    1.如何退出while , 现在是报错退出; ctrl+C 退出如何不报错.
	2.如何到80行后自动回到第一行
"""


"""
>>> import os
>>> os.system('clear')
"""
