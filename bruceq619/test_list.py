testList = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
testList2 = ["zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"]
testList3 = "abcdefghij"

outcomeList = testList3[:]

print(outcomeList)
print(outcomeList[-1])

print(outcomeList[:5])

print(outcomeList[5:])

print(outcomeList[::2])

print(outcomeList[::-1])

print(outcomeList[-5: -1])

print(outcomeList[-5: 0])

print(outcomeList[-5: -8])

print(outcomeList[-5: 1])
